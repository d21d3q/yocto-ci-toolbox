FROM ubuntu:jammy

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade \
    && apt-get install -y curl lsb-core ripgrep


RUN apt-get install -y gawk wget git diffstat unzip texinfo gcc build-essential \
    chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils \
    iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev python3-subunit \
    mesa-common-dev zstd liblz4-tool file locales libacl1

# documentation
RUN apt-get install -y make python3-pip  \
    && pip3 install sphinx sphinx_rtd_theme pyyaml

# Additional host packages required by poky/scripts/wic
# RUN apt-get install -y dosfstools mtools parted syslinux tree zip

# install additional tools
# awscli, azure-cli, just, repo, rust
RUN pip3 install pip awscli --upgrade  && \
    pip3 install boto3 && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    curl -s 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null && \
    echo "deb [arch=all,$(dpkg --print-architecture) signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" | tee /etc/apt/sources.list.d/prebuilt-mpr.list   && \
    apt-get update && \
    apt-get install -y just && \
    curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
    chmod a+x /usr/local/bin/repo && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Fix error "Please use a locale setting which supports utf-8."
# See https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues
RUN apt-get install -y locales  && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
        echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
        dpkg-reconfigure --frontend=noninteractive locales && \
        update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN useradd --uid 1000 --create-home build

USER build

WORKDIR /workspace

CMD "/bin/bash"

